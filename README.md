# docker-puppet

[![build status](https://gitlab.com/jonadupio/docker-puppet/badges/master/build.svg)](https://gitlab.com/jonadupio/docker-puppet/commits/master)

Use this image to test your puppet code

## Tools included

- git
- puppet
- wget
- puppet-linter

## Puppet modules included

- puppetlabs-concat

## Usage

### Pull the container

#### Centos
```
docker pull registry.gitlab.com/jonadupio/docker-puppet:centos
```
#### Debian
```
docker pull registry.gitlab.com/jonadupio/docker-puppet:debian
```

### Use the container
Run the container with your code:
Go to your puppet module folder
```
docker run --rm -v $PWD:/root/puppet/ registry.gitlab.com/jonadupio/docker-puppet:<os> <command>
```

#### Commands

### Parser
This command will perform a "puppet parser validate" on your puppet code
```
docker run --rm -v $PWD:/root/puppet/ registry.gitlab.com/jonadupio/docker-puppet:<os> parser
```

### Linter
This command will perform a "puppet-lint" on all your puppet code
```
docker run --rm -v $PWD:/root/puppet/ registry.gitlab.com/jonadupio/docker-puppet:<os>t linter
```

### Apply
This command will try to execute your puppet code
```
docker run --rm -v $PWD:/root/puppet/ registry.gitlab.com/jonadupio/docker-puppet:<os> apply
```
Don't forget to put some manifest file on your module folder

### Version
This command will show the version of puppet

```
docker run --rm -v $PWD:/root/puppet/ registry.gitlab.com/jonadupio/docker-puppet:<os> version
```

#### Alias
You can set this alias to use the container more easily
```
alias puppet='docker run --rm -v $PWD:/root/puppet registry.gitlab.com/jonadupio/docker-puppet:centos'
```

you can test with
```
puppet version
```